# Handbook KawalCOVID19

## Apa ini

Dokumen ini adalah dokumen acuan (overview, architecture, timeline, etc) dalam pengembangan berbagai perangkat lunak yang tergabung dalam naungan KawalCOVID19 mulai dari website [kawalcovid19.id](https://kawalcovid19.id), mobile apps, hingga berbagai aplikasi web dari tim lainnya. Semua artifak di repo ini bersifat belum final dan bisa berubah-ubah sewaktu mengikuti perkembangan situasi dan kebutuhan.

Untuk kalian yang pertama kali membuka handbook ini, pastikan kalian membaca [overview](./overview.md) dan [how we work](./how-we-work.md) untuk bisa mulai berkontribusi.

## Daftar Isi

- [Overview](./overview.md)
- [How we Work](./how-we-work.md)
- [Architecture](./architecture.md)
- [Content Release Workflow](./workflow-content/0-general-overview.md)
- [Wishlist](./wishlist.md)
- [Cara berkontribusi](./CONTRIBUTING.md)
- [Penggunaan dan *setup* *link shortener* kcov.id](./kcovid-usages.md)
- [Penggunaan dan *setup* `GitLab Slack application` dan `GitLab Slack notification`](./gitlab-slack-integration.md)

## Team dan Expert

Di workspace Slack kita (kawalcovid19) anggota dibagi dan dapat memasuki channel sesuai dengan fungsionalitas/vertical (#team-{nama-tim}) atau kompetensi/expertise (#expert-{nama-kompetensi}).

Daftar #team- sejauh ini yaitu:

- #team-analytics
- #team-content | PIC: Christopher Angelo & Resi Respati | [workflow](./Workflow%20Content/.%20General%20Overview.md)
- #team-crowdsource-datavis
- #team-crowdfunding-salingjaga
- #team-data-eng
- #team-hardware-ventilator
- #team-kawalrumahsakit-crowdsource-dari-rs | PIC: Felix Halim | [GitLab Subgroup](https://gitlab.com/kawalcovid19/crowdsource-rs)
- #team-handbook |PIC: Handbook Maintainers (@handbook)
- #team-kawal-diri | PIC: Adhi Utama | [GitLab Subgroup](https://gitlab.com/kawalcovid19/kawal-diri)
- #team-kcapps-android | PIC: Javentira, Andri Suranta Ginting, Raditya Gumay | [GitLab Project](https://gitlab.com/kawalcovid19/mobile/kawalcovid19-android)
- #team-kcapps-ios | PIC: Budi Oktaviyan,  Fandy Gotama| [GitLab Project](https://gitlab.com/kawalcovid19/mobile/kawalcovid19-ios)
- #team-needs-matchmaking
- #team-portal-kemendikbud | PIC: Ibrahim Arief | DORMANT
- #team-self-diagnose | PIC: Pandu Putra | [GitLab Subgroup](https://gitlab.com/kawalcovid19/self-diagnose)
- #team-tech-admin
- #team-whatsapp
- #team-website
- #team-website-api

Current progress dari setiap tim dapat dilihat baik langsung di repository-nya, wiki repository, ataupun conversation dan pinned message di channel tim.

Sedangkan untuk #expert- yaitu:

- #expert-backend | PIC: Edwin Lunando
- #expert-data-analytics | PIC: Imam Abdul Rahman
- #expert-data-engineering | PIC: Bayu Aldiyansyah
- #expert-design
- #expert-frontend | PIC: Resi Respati
- #expert-gis | PIC: Ajeng Salma Yarista
- #expert-handbook | PIC: Handbook Maintainers
- #expert-legal
- #expert-mobile | PIC: Ibnu Sina Wardy | [GitLab Subgroup](https://gitlab.com/kawalcovid19/mobile)
- #expert-pm | [Single Source of Truth](http://kcov.id/product-ssot)
- #expert-sosmed | PIC: Avina Nadhila Widarsa
- #expert-qa | PIC: Aditya Amin Refandi

PIC masing2 tim (#team-) sejauh ini masih mengecek kebutuhan di tiap tim seperti apa dan nanti lebih lanjut akan mencari team members di setiap grup #expert- nya.

## Job Posting or Job Seeking

Melalui [laman ini](./job-posting/job-available.md) untuk rekan-rekan PIC maupun Product Manager (PM) dapat memposting posisi yang dibutuhkan

Untuk menampilkan posisi yang dibutuhkan silahkan gunakan frasa `dibutuhkan` posisi, **contoh dibutuhkan seo**

Sedangkan bagi rekan-rekan lainnya yang belum mempunyai tugas bisa menghubungi tim @handbook agar dihubungkan dengan PIC atau PM melalui [laman ini](./job-posting/volunteer-available.md). Bisa juga secara proaktif kirim pesan ke channel dengan frasa `available` posisi, **contoh available backend**

## Update Team

Untuk rekan-rekan PIC yang ingin membuat channel baru diharuskan untuk selalu berkoordinasi dengan #team-handbook agar jumlah tim selalu terupdate pada laman ini. Silahkan hubungi dengan mention **@handbook** di slack agar kami bisa memperbaruinya.  

Hal ini juga berlaku untuk setiap perubahan nama atau penghapusan *nama channel* dari `slack`

## Meeting Board

Berisi jadwal meeting yang akan dilakukan dan [dokumentasinya](./meeting.md)
