# Cara Kerja

## Metode

Kita membuat kemajuan dengan tentunya mengerjakan pekerjaannya dan yang tidak kalah pentingnya juga dengan memperbarui status di Gitlab. Berikut beberapa contoh kemajuan yang dasar:

- **Melaporkan masalah** dalam bentuk Gitlab Issue
- Menugaskan diri sendiri sebagai tanda **mengambil tanggung jawab** dalam sebuah masalah
- Mengirimkan merge request untuk **menawarkan solusi** untuk sebuah masalah
- **Meminta review** kepada minimal satu expert yang setipe untuk setiap merge request yang ada
- Reviewer **mengkonfirmasi hasil review** sesuai panduan [code review](https://google.github.io/eng-practices/review/) dengan mengomentari merge request

Semua progress dan komunikasi harus secara disiplin terdokumentasikan di GitLab agar kita semua punya visibilitas akan kemajuan yang sudah kita lakukan. KawalCovid19 menerapkan [Gitlab WorkFlow](./gitlab-development-guide.md) dalam management development software.

## Otomasi

Untuk setiap repo yang bisa di-deploy, pastikan menggunakan [CI/CD pipeline Gitlab](https://docs.gitlab.com/ee/ci/pipelines/). Kalau di repo tersebut belum ada pipeline untuk deploy, buatlah terlebih dahulu sebelum deploy. Dengan membuat pipeline deploy, kita sekaligus mendokumentasikan cara men-deploy aplikasi kita dan menghemat waktu di jangka panjang untuk tidak men-deploy aplikasi secara manual.

## Komunikasi

Menulis, menulis, dan menulis. Untuk semua organisasi yang mengadopsi cara kerja *full-remote*, utamakan komunikasi asinkron, entah itu di Slack atau Gitlab. Dengan semua diskusi itu ditulis dan terbuka ke semua, orang yang terlibat dengan diskusinya pun mendapat konteksnya. Bahkan, orang-orang yang baru masuk pun jadi bisa mulai mengikuti perkembangan yang ada di organisasi ini. Oleh karena itu, tulislah semua hal, termasuk hasil diskusi yang berasal dari video call. Untuk panduan komunikasi yang efektif, bisa mengikuti [panduan komunikasi Gitlab](https://about.gitlab.com/handbook/communication/#effective--responsible-communication-guidelines).

Jika dirasa sebuah diskusi sudah terhambat dalam jangka waktu yang lumayan lama, gunakan jalur komunikasi sinkron seperti Slack atau Zoom. Untuk kasus-kasus yang dirasa membutuhkan diskusi yang ketat, langsung saja menggunakan komunikasi sinkron, dengan menuliskan hasil diskusinya. Salah satu aturan yang cukup membantu itu, kalau kamu sudah 3 kali bolak balik di diskusi yang sama tanpa mengarah ke solusi yang jelas, langsung ajak video call untuk meluruskan.
