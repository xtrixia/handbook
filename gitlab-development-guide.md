# Pengembangan Software di Gitlab

## Gitlab Overview

[Gitlab](https://about.gitlab.com) merupakan source code management berbasis web. Tidak hanya itu, Gitlab juga menyediakan berbagai tools untuk kemudahan pengembangan Software. Diantaranya ialah issue-tracking, CI/CD pipelines dan git-repository. KawalCovid19 menggunakan gitlab sebagai management pengembangan Software dan juga untuk kordinasi teknis. Gambaran umum hal teknis pengembangan software di Gitlab bisa Anda temukan pada [laman ini](https://about.gitlab.com/blog/2016/03/08/gitlab-tutorial-its-all-connected).

## Definisi Umum

Ada beberapa istilah teknis terkait pengembangan Software di Gitlab. Beberapa mungkin akan sering dijumpai oleh para kontributor. Diantaranya ialah:

- **Issue** : Identifikasi masalah dari proses development. Tiap kontributor dapat membuat issue baru ketika menemukan masalah dalam proses development.
- **Merge Request**: Permintaan penggabungan atau persetujuan perubahan dari kontributor.
- **Epic** : Issue-issue yang saling berkaitan.
- **WIP** : _Work in Progres_ artinya pekerjaan masih dalam tahap penyelesaian.
- **Milestone** : Monitoring proses development. Penentuan target development untuk siap dirilis.
- **Label** : Penanda untuk memudahkan kontributor mengkategorikan status dari Issue atau pun Merge Request.

## Gitlab Workflow

### Membuat Issue

Untuk mulai berkontribusi, carilah issue yang akan diselesaikan. Jika belum ada pada list issue, silahkan buat issue baru. Standart Gitlab Workflow selalu menyarankan untuk selalu mulai dengan issue. Issue merupakan [Single Source of Truth](https://about.gitlab.com/blog/2016/03/03/start-with-an-issue). Artinya, semua bentuk penyelesaian dan kordinasi acuannya dari issue.

![Contoh GitLab Issue](./images/gitlab-development-guide-issue.png)

### Mengirim Merge Request

Setelah Anda memutuskan untuk menyelesaikan suatu issue, langkah selanjutnya mulai bekerja untuk berubahan yang ditawarkan dan buat Merge Request Anda. Biasanya, Anda akan ditawari untuk membuat Merge Request dari issue terkait. Anda juga akan dibuatkan branch dengan penomoran dan nama sesuai issue terkait.

![Contoh GitLab Merge Requesti Dari Issue](./images/gitlab-development-guide-merge-request-issue.png)

List Merge Request yang telah dikirimkan untuk di-review.

![Contoh GitLab Merge Request](./images/gitlab-development-guide-merge-request.png)

### Mereferensikan Issue dan Merge Request

Untuk memudahkan tracking suatu Issue dan penyelesaiannya, Gitlab memiliki mekanisme referensi. Dengan mereferensikan Merge Request ke Issue terkait, banyak kemudahan yang bisa dicapai. Salah satunya ketika Merge Request diterima, Issue akan secara otomatis ditutup ketika menambahkan kata khusus yang dikenali Gitlab (Fixes, Closees) diikuti referensi. Referensi dapat ditulis di komen, konten utama maupun commit message. Referensi di Gitlab memiliki penulisan khusus.

- Simbol `!` menandakan referensi ke Merge Request. Contohnya `!123` merujuk ke Merge Request dengan nomer 123.
- Simbol `#` menandakan referensi ke Issue. Contohnya `#124` merujuk ke Issue dengan nomer 124.
- Simbol `$` menandakan referensi ke Snippet. Contohnya `$300` merujuk ke Snippet dengan nomer 300.

![Contoh GitLab Reference](./images/gitlab-development-guide-issue-simbol.png)
