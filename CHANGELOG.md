# Changelog

Dokumen yang berisi changelog dari perubahan keputusan arsitektur aplikasi.\
Dibuat untuk mengantisipasi supaya checkpoint-checkpoint penting terkait arsitektur tidak tenggelam di antara cairnya perkembangan situasi.

Format yang digunakan mengikuti [Keep a Changelog](https://keepachangelog.com/id-ID/1.0.0/)

## 2020-03-04

### Ditambahkan

- Website Utama

  - Website Utama sudah up di [kawalcovid19.id](https://kawalcovid19.id)
  - Website menggunakan Headless Wordpress dengan NextJS sebagai frontendnya
  - Static site untuk Website dideploy di Netlify
  - Headless WP dideploy di Azure
  - Storage dan DB menggunakan Azure

- Link Shortener
  - Link Shortener sudah up di [kcov.id](https://kcov.id)
  - Dideploy di Netlify
  - Link Shortener dibuat menggunakan [netlify-shortener](https://github.com/kentcdodds/netlify-shortener) & [Netlify's `_redirects`](https://www.netlify.com/docs/redirects/).
  - Untuk menambahkan link tinggal tambahkan entri baru di [kcov.id/shorten](https://kcov.id/shorten), lalu _commit_ dan _push_ ke _branch_ `master`

## 2020-03-02

- Website Utama sudah up di [kawalcovid19.id](https://kawalcovid19.id)
- Website menggunakan Headless Wordpress dengan Gatsby.js sebagai frontendnya
- Website dideploy di Netlify
- Headless WP dideploy di Heroku dan menggunakan Amazon S3 sebagai build source
