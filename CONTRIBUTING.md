# Cara Berkontribusi

## Panduan Mulai Bekerja

### Kontribusi Metode dan Pengalaman

1. Join channel expert sesuai expertisenya dengan cari channel berawalan expert-[expertise Anda]
   1. [#expert-backend](https://app.slack.com/client/TUUBXJD8R/C010384HGTY)
   2. [#expert-data-analytics](https://app.slack.com/client/TUUBXJD8R/C0103CLBMC6)
   3. [#expert-frontend](https://app.slack.com/client/TUUBXJD8R/C0102TBJFBR)
   4. [#expert-gis](https://app.slack.com/client/TUUBXJD8R/CV6KJ55MJ)
   5. [#expert-devops](https://app.slack.com/client/TUUBXJD8R/C0106F8GZAT)
   6. [#expert-mobile](https://app.slack.com/client/TUUBXJD8R/CUS5Z17MG)
   7. Sisanya bisa dicari dengan awalan `expert`
2. Akan ada pertanyaan dan bahasan yang secara sporadis dimulai. Anda bisa berkontribusi di sana

Kontribusi ini bersifat pasif karena menunggu kebutuhan.

### Kontribusi Pengembangan

Untuk kontribusi aktif, KawalCOVID19 tentunya membutuhkan engineer untuk mengembangkan aplikasi yang ditujukan untuk membantu penanganan COVID-19.

1. Pilih tim tempat anda berkontribusi, saat ini ada 17 tim (jumlah tim bersifat dinamis sesuai kebutuhan), dengan cari channel berawalan `team`
2. Kontak PIC task existing atau baca kebutuhan di channel saat ini
3. Kunjungi laman issues Gitlab dari tim Anda dan bisa memulai meng-assign issues. Berikut contoh dari [laman issues team self diagnose](https://gitlab.com/kawalcovid19/self-diagnose).

**Untuk kalian yang ingin secara langsung proaktif berkontribusi,** bisa langsung ke [laman issues GitLab](https://gitlab.com/groups/kawalcovid19/-/issues) dan kontribusi di sana.\
Permintaan akses _push_ ke _repository_ bisa langsung dilakukan dengan cara _request access_ di GitLab Subgroups tim terkait untuk nantinya disetujui oleh pengelola tim tersebut. Jangan lupa juga isi [formulir pendataan relawan ini](http://kcov.id/formtim) apabila belum mengisinya.

Dalam proyek ini setiap ada issues dan mengerjakan issue silahkan gunakan GitLab agar semua aktivitas dapat terdokumentasi dan dapat diikuti kemajuan pengerjaannya dari waktu ke waktu.

Sebelum mengerjakan sebuah _issue_, ada beberapa hal yang harus diperhatikan:

1. Silahkan isi kolom *assignee* dengan @username masing-masing
2. Ubah label agar rekan yang lain mengerti status suatu issues. Jika dikerjakan lebih dari satu orang dapat menggunakan fitur [*multiple assignee*](https://docs.gitlab.com/ee/user/project/issues/multiple_assignees_for_issues.html) untuk menunjuk beberapa *assignee* sekaligus.
3. Untuk memulai proyek, pastikan **_create branch_** dari `master`, lalu segera *commit* dan *push* perubahannya sekecil apapun itu sebelum meninggalkannya. Kemudian **buat _merge request_** baru dengan menambahkan awalan `WIP:` di judulnya untuk [menandainya sebagai _work in progress_](https://docs.gitlab.com/ee/user/project/merge_requests/work_in_progress_merge_requests.html). Dengan demikian, orang lain yang ingin mengerjakan _issue_ yang sama akan berpikir dua kali sebelum ikut mengerjakannya.
4. Untuk Mengetahui cara kerja kita silakan klik [di sini](./how-we-work.md)
5. Usahakan untuk selalu merekam setiap rapat yang dilakukan, mengunggahnya ke YouTube, dan menambahkannya ke dalam [*YouTube playlist* ini](https://kcov.id/youtube) kita serta menambahkan videonya [ke handbook kita](./Meeting.md) supaya relawan-relawan lainnya dapat mengikuti kemajuan pembahasan dan pengambilan keputusan di KawalCOVID19.
6. KawalCOVID19 [URL shortener](https://gitlab.com/kawalcovid19/website/kcov.id)

## Alur Kerja

```mermaid
stateDiagram
	[*] --> Open
	Open --> Discussion
	Discussion --> Ready
	Open --> Ready
	Ready --> Doing
	Open --> Doing
	Doing --> Blocked
	Blocked --> Doing
	Doing --> Closed
	Doing --> Invalid
	Closed --> [*]
	Invalid --> [*]
```
