// see all config {@link} https://vuepress.vuejs.org/theme/default-theme-config.html
module.exports = {
  title: 'KawalCOVID19',
  description: 'Buku Pegangan Komunitas KawalCOVID19',
  base: '/',
  theme: 'cool',
  head: [
    ['link', { rel: 'icon', href: '/favicon.ico' }],
  ],
  themeConfig: {
    logo: 'favicon-32x32.png',
    nav: require('./nav'),
    sidebar: 'auto',
    searchPlaceholder: 'Tekan "/" untuk fokus',
    lastUpdated: 'Terakhir diperbarui',
    repo: 'https://gitlab.com/kawalcovid19/handbook',
    editLinks: true,
    editLinkText: 'Edit halaman ini',
    markdown: {
      extendMarkdown: md => {
        md.use(require('markdown-it-task-lists'), {
          enabled: true,
          label: true,
          labelAfter: true,
        })
      },
    },
  },
  configureWebpack: {
    resolve: {
      alias: {
        '@images': '../images'
      }
    }
  },
  plugins: [
    '@vuepress/back-to-top',
    [
      '@vuepress/pwa', {
        serviceWorker: true,
        updatePopup: true
      }
    ],
    [
      'vuepress-plugin-medium-zoom', {
        selector: 'img',
      }
    ],
    [
      'mermaidjs', {
        themeCSS: '.label foreignObject { overflow: visible; font-size: 12px}'
      }
    ]
  ],
}
